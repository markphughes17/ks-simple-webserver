# ks-simple-webserver

Project to use terraform to build a simple linux webserver 

## Prerequisites
# Prerequisites
In order to deploy or run this application the following will be required.

- Terraform version 1.0+
- AWSCLI
- Ansible


### To deploy AWS Resources
This instruction assumes you have an AWS account with access keys configured in your terminal.

- terraform apply --var-file vars/variables.tfvars

This creates the basic EC2 instance and other required resources, and outputs the DNS Name and IP Address, which is needed to run the ansible

### To Install NGINX and index.html to instance

- Copy the DNS name from the previous step into line 2 of the hosts file (ansible/hosts).
- It is best to ensure instance status checks have completed before running the next command.
- ansible-playbook -i ansible/hosts ansible/web.yml

### Next steps/improvements
- Time allowing, I'd use a gitlab pipeline job to fully automate the deployment of this. 
- If this were a real server with real traffic going to it, I'd also use a load balancer and ASG in front of the webserver.
- Register a domainn name and static public IP address so that the same address persists between rebuilds

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.53.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eip.web](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_instance.web](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_key_pair.web](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/key_pair) | resource |
| [aws_security_group.allow_all](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ami_id"></a> [ami\_id](#input\_ami\_id) | AMI used to build server(s) | `string` | n/a | yes |
| <a name="input_private_ip"></a> [private\_ip](#input\_private\_ip) | Private IP address for server | `string` | n/a | yes |
| <a name="input_ssh_public_key"></a> [ssh\_public\_key](#input\_ssh\_public\_key) | Public key used for SSH access to server | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_DNS"></a> [DNS](#output\_DNS) | n/a |
| <a name="output_Public_IP"></a> [Public\_IP](#output\_Public\_IP) | n/a |