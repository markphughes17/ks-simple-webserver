terraform {
    backend "s3" {
        bucket = "mph-terraform-state"
        key = "ks-simple-webserver"
        region = "eu-west-2"
    }
}
