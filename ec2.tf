resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = "t2.micro"

  key_name = aws_key_pair.web.key_name
  private_ip = var.private_ip

  vpc_security_group_ids = [aws_security_group.allow_all.id]

  tags = {
    Name = "HelloWorld"
  }

}

resource "aws_eip" "web" {
  instance = aws_instance.web.id
  vpc      = true
}

resource "aws_key_pair" "web" {
  key_name   = "webserver-key"
  public_key = var.ssh_public_key
}

output "DNS" {
  value = aws_eip.web.public_dns
}

output "Public_IP" {
  value = aws_eip.web.public_ip
}

resource "aws_security_group" "web_sg" {
  name        = "web_sg"
  description = "Allow SSH access from my IP and public access to http"


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["86.137.249.70/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_all"
  }
}


