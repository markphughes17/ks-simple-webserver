terraform {
    required_version = "~>1.0.4"
}


provider "aws" {
    region = "eu-west-2"
}
