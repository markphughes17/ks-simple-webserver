variable "ami_id" {
  description = "AMI used to build server(s)"
  type = string
}

variable "ssh_public_key" {
  description = "Public key used for SSH access to server"
  type = string
}

variable private_ip {
  description = "Private IP address for server"
  type = string
}